open Printf
module Array = ArrayLabels

let prf fmt = ksprintf (printf "%s%!") fmt
let line fmt = ksprintf (prf "%s\n%!") fmt

let test_jack_seq () =
  let open Misuja in
  let seq =
    let input_ports = Array.init 10 ~f:(sprintf "in%d") in
    let output_ports = Array.init 10 ~f:(sprintf "out%d") in
    Sequencer.make ~name:"JackSeqTest" ~input_ports ~output_ports
  in
  for i = 0 to 25000000 do
    Thread.delay 0.02 ;
    let input = Sequencer.get_input seq in
    Array.iter input ~f:(fun (port, stat, chan, dat1, dat2) ->
        Printf.printf "[%d] port:%d stat:%x chan:%d dat1:%d dat2:%d\n%!" i port
          stat chan dat1 dat2 ;
        let high_level =
          match stat with
          | rs when 0x80 <= rs && rs <= 0x8F -> `Note_off (rs, dat1, dat2)
          | rs when 0x90 <= rs && rs <= 0x9F ->
              if dat2 = 0 (* If velocity = 0 -> note off ! *) then
                `Note_off (rs, dat1, dat2)
              else `Note_on (rs, dat1, dat2)
          | _other -> `None
        in
        match high_level with
        | `None -> Sequencer.output_event seq ~port ~stat ~chan ~dat1 ~dat2
        | `Note_on (_, dat1, dat2) | `Note_off (_, dat1, dat2) ->
            Sequencer.output_event seq ~port ~stat ~chan ~dat1 ~dat2 ;
            Sequencer.output_event seq ~port ~stat ~chan ~dat1:(dat1 + 7) ~dat2 ;
            Sequencer.output_event seq ~port ~stat ~chan ~dat1:(dat1 + 12)
              ~dat2 ;
            Sequencer.output_event seq ~port ~stat ~chan ~dat1:(dat1 + 19)
              ~dat2 ;
            Sequencer.output_event seq ~port ~stat ~chan ~dat1:(dat1 + 24)
              ~dat2 )
  done ;
  Sequencer.close seq ;
  Unix.sleep 3 ;
  ()

let () = test_jack_seq ()
